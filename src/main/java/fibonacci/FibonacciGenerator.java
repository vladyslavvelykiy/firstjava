package fibonacci;

/**
 * Class that generates Fibonacci sequence
 */
public class FibonacciGenerator {
    /**
     *This variable is constant and determines default count of sequence members
     */
    private static final int defaultCountOfEl = 11;
    /**
     * This variable determines a first element of Fibonacci sequence
     */
    private static int firstElement = 1;
    /**
     * This variable determines a second element of Fibonacci sequence
     */
    private static int secondElement = 1;

    /**
     * Constructor for its class
     */
    public FibonacciGenerator() {
    }

    /**
     *Method creates new array with lenth that is equal to number of element and invokes the method
     * of generated sequence
     * @param countNumbs Accepts the number of elements to generate. The number of items must be greater than 2
     * @return Returns generated Fibonacci sequence as int array
     */
    public int[] generateFibonacci(int countNumbs) {
        int[] array = new int[countNumbs];
        return doFibonacciOperations(array);
    }

    /**
     * Method creates new array with default lenth  and invokes the method  of generated sequence
     * @return Returns generated Fibonacci sequence as int array
     */
    public int[] generateFibonacci() {
        int[] array = new int[defaultCountOfEl];
        return doFibonacciOperations(array);
    }

    /**
     * Performs an algorithm for Fibonacci sequence
     * @param array Accept array to write a sequence
     * @return Returns array with writed sequence
     */
    private int[] doFibonacciOperations(int... array) {
        int n2;
        array[0] = 1;
        array[1] = 1;

        for (int i = 2; i < array.length; i++) {
            n2 = firstElement + secondElement;
            array[i] = n2;
            firstElement = secondElement;
            secondElement = n2;
        }
        return array;
    }


    @Override
    public String toString() {
        return "FibonacciGenerator{" +
                "defaultNum=" + defaultCountOfEl +
                '}';
    }
}


